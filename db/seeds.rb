# encoding: utf-8

Image.delete_all

Image.create(:folder=>"Prod/Images", :name=>'vcl-bitnami-lamp', :pretty_name=>'LAMP Stack', :desc => "BitNami LAMP "+
"Stack provides a complete PHP, MySQL and Apache development environment for Linux that can be launched in one "+
"click. It also bundles phpMyAdmin, SQLite, Varnish, ImageMagick, ModSecurity, XDebug, Xcache, OAuth, Memcache, "+
"FastCGI, APC, GD, OpenSSL, CURL, openLDAP, PEAR, PECL and other components and the following frameworks: "+
"Zend Framework, Symfony, CodeIgniter, CakePHP, Smarty, Laravel.")

Image.create(:folder=>"Prod/Images", :name=>'vcl-bitnami-ruby', :pretty_name=>'Ruby Stack', :desc => "BitNami "+
"Ruby Stack provides a complete development environment for Ruby on Rails that can be deployed in one click. "+
"It includes the latest stable release of Ruby, RVM, Rails, Apache, NGinx, MySQL, SQLite, Git and Subversion,"+
" Memcache and Varnish, Sphinx, PHP and phpMyAdmin. In addition to the base Ruby runtime and libraries, the stack"+
" includes most popular gems for building Rails applications: Passenger, Nokogiri, Rake, RMagick, Mongrel, "+
"Thin and more. It also includes additional libraries such as ImageMagick, OpenSSL, CURL, and openLDAP.")

Image.create(:folder=>"Prod/Images", :name=>'vcl-bitnami-node', :pretty_name=>'Node.js', :desc => "Node.js is a"+
" platform built on Chrome’s JavaScript runtime for easily building fast, scalable network applications. "+
"Node.js uses an event-driven, non-blocking I/O model that makes it lightweight and efficient, perfect for "+
"data-intensive real-time applications that run across distributed devices.")

Image.create(:folder=>"Prod/Images", :name=>'vcl-bitnami-tomcat', :pretty_name=>'Tomcat', :desc => "Apache Tomcat"+
" implements the Servlet and JavaServer Pages specifications from the Java Community Process. It includes many"+
" additional features that make it a useful platform for developing and deploying web applications and web "+
"services.")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-wordpress',
:pretty_name =>'WordPress',
:desc => "WordPress is one of the world’s most popular web publishing platforms for building blogs and websites. It can be customized via a wide selection of themes, extensions and plug-ins.")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-django',
:pretty_name =>'Django',
:desc => "Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design. Developed and used over two years by a fast-moving online-news operation, Django was designed to handle two challenges: the intensive deadlines of a newsroom and the stringent requirements of the experienced Web developers who wrote it. It lets you build high-performing, elegant Web applications quickly.")

Image.create(:folder=>'', :name=>'other', :pretty_name=>'A Bitnami VM not listed here', :desc =>
"Please indicate which Bitnami Server you want. Please be aware this will cause a one business day delay in "+
"getting your VM. Visit http://bitnami.org to see your options.  Please indicate your choice")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-omeka',
:pretty_name =>'Omeka',
:desc => "Omeka is a free, flexible, and open source web-publishing platform for the display of library, museum, archives, and scholarly collections and exhibitions. Its \“five-minute setup\” makes launching an online exhibition as easy as launching a blog.")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-drupal',
:pretty_name =>'Drupal',
:desc => "Drupal is a content management platform that allows users to easily publish, manage, and organize a wide variety of content on a website. It is used for community web portals, discussion sites, corporate web sites, intranet applications and many other sites. Drupal is easy to extend by plugging in freely available modules.")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-mediawiki',
:pretty_name =>'MediaWiki',
:desc => "MediaWiki is a wiki package originally written for Wikipedia. MediaWiki is an extremely powerful, scalable software and a feature-rich wiki implementation.")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-matlab',
:pretty_name =>'MATLAB',
:desc => "MATLAB® is a high-level language and interactive environment for numerical computation, visualization, and programming. Using MATLAB, you can analyze data, develop algorithms, and create models and applications. The language, tools, and built-in math functions enable you to explore multiple approaches and reach a solution faster than with spreadsheets or traditional programming languages, such as C/C++ or Java™")

Image.create(:folder=>'Prod/Images',:name=>'vcl-bitnami-bioinfomatics',
:pretty_name =>'Bioinformatics (iPython/Anaconda)',
:desc => "Research Computing, Center for Systems Biology, and IGSP scientific computing short course image")

ReservationState.delete_all
ReservationState.delete_all
ReservationState.create(:name=>"Waiting for New Image",:description=>"This is a new Bitnami image which must be "+
"added to the system")
ReservationState.create(:name=>"Reserved",:description=>"VM is active and in use")
ReservationState.create(:name=>"Expired",:description=>"Reservation is over and VM has been destroyed")
ReservationState.create(:name=>"Expiration Pending",:description=>"Reservation is over and VM is slated for removal")
ReservationState.create(:name=>"ERROR",:description=>"An error occurred - Contact OIT")
ReservationState.create(:name=>"Build Pending",:description=>"VM is ready to be created")
ReservationState.create(:name=>"Building",:description=>"VM is being created")
ReservationState.create(:name=>"Network Pending",:description=>"VM is ready to be added to the network")
ReservationState.create(:name=>"Networking",:description=>"VM is being added to the network")
ReservationState.create(:name=>"Waiting DHCP Propagation",:description=>"Waiting for Network information to propogate")
ReservationState.create(:name=>"Finishing Up",:description=>"Powering on and setting password")
ReservationState.create(:name=>"Deprovision Pending",:description=>"VM and its network info are ready to be deprovisioned")
ReservationState.create(:name=>"Deprovisioning",:description=>"VM and its network are being deprovisioned")
ReservationState.create(:name =>'Export Pending', :description=>"Request to export VM is pending")
ReservationState.create(:name =>'Exporting', :description=>"VM source is being exported")

NetworkResource.delete_all
User.delete_all
VmRequest.delete_all
Vm.delete_all

nr = NetworkResource.create(:dns=>'colab-sbx-01.oit.duke.edu', :ip=>'152.3.53.2',:mac_addr=>'00:50:56:bb:69:51')
uid = User.create(:duid => "0435319", :netid => "mccahill", :displayName=> "Mark McCahill", :phone=> "+1 919 684 6638", 
  :email=> "mark.mccahill@duke.edu", :vms_allowed=> 2)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-node').id, 
  admin_password: "n2clegodry")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "mccahill", technical_contact_name: "Mark McCahill",
 other_contacts: "mccahill", proj_name: "CoLab support and test", tandc: true, image_name: "vcl-bitnami-node", 
 info: nil, hostname: "colab-sbx-01.oit.duke.edu", description: "used by Mark McCahill for testing/support of CoLab", 
 shortname: "colab-sbx-01.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
  vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")

nr = NetworkResource.create(:dns=>'colab-sbx-02.oit.duke.edu', :ip=>'152.3.53.10',:mac_addr=>'00:50:56:BB:02:4D')
uid = User.create(:duid => "0587090", :netid => "dcg13", :displayName=> "Davis Gossage", :phone=> "+1 704 728 2460", 
  :email=> "dcg13@duke.edu", :vms_allowed=> 2)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-lamp').id, admin_password: "WhadInEyfs")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "dcg13", technical_contact_name: "Davis Gossage",
 other_contacts: "Emmanuel Shiferaw / Robert Ansel / Jason Oettinger", proj_name: "DevilPrint", tandc: true, image_name: "vcl-bitnami-node", 
 info: "We are requesting a clean install of the bitnami LAMP stack on our already existing VM found at colab-sbx-02.oit.duke.edu.", 
 hostname: "colab-sbx-02.oit.duke.edu", description: "Mobile ePrinting solution", 
 shortname: "colab-sbx-02.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
  vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")
	

nr = NetworkResource.create(:dns=>'colab-sbx-03.oit.duke.edu', :ip=>'152.3.53.6',:mac_addr=>'00:50:56:BB:0A:57')
uid = User.create(:duid => "0502355", :netid => "yz96", :displayName=> "Jonathan Zhang", :phone=> "+1 919 660 7372", 
  :email=> "yz96@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-node').id, 
  admin_password: "wh0knows2")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "yz96", technical_contact_name: "Jonathan Zhang",
 other_contacts: "", proj_name: "ISIS Project Prototyping", tandc: true, image_name: "vcl-bitnami-node", 
 info: 'MangoDB', hostname: "colab-sbx-03.oit.duke.edu", description: "For ISIS 495 capstone - DukeArts project", 
 shortname: "colab-sbx-03.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")
  
nr = NetworkResource.create(:dns=>'colab-sbx-04.oit.duke.edu', :ip=>'152.3.53.7',:mac_addr=>'00:50:56:bb:5F:92')
uid = User.create(:duid => "", :netid => "sk317", :displayName=> "Suyash Kumar", :phone=> "+1 561 400 2423", 
  :email=> "suyash.kumar@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-ruby').id, 
  admin_password: "wh0knows23")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "sk317", technical_contact_name: "Suyash Kumar",
 other_contacts: "", proj_name: "Duke Share", tandc: true, image_name: "vcl-bitnami-ruby", 
 description: "College campuses are places where there's an incredible concentration of diverse talent...",
 hostname: "colab-sbx-04.oit.duke.edu",
 shortname: "colab-sbx-04.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
  vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")

nr =NetworkResource.create(:dns=>'colab-sbx-05.oit.duke.edu', :ip=>'152.3.53.8',:mac_addr=>'00:50:56:bb:0C:D2')
uid = User.create(:duid => "0555203", :netid => "hcd7", :displayName=> "Holly Davis", :phone=> "+1 206 883 5877", 
  :email=> "holly.davis@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-ruby').id, 
  admin_password: "wh0kn3ss23")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "hcd7", technical_contact_name: "Holly Davis",
 other_contacts: "Ben Donnelly in the Marine Geospatial Ecology Laboratory in the Nicholas School of the Environment", 
 proj_name: "Mobile App for Sea Turtle Data Collection", tandc: true, image_name: "vcl-bitnami-ruby", 
 description: " With the advent of mobile devices, opportunities exist for increased sea turtle data collection.  I am in the process of creating a smartphone and tablet application specifically designed for collecting sea turtle data.  The planned data fields would match the data collected for nesting and stranding events by SeaTurtle.org for the state of North Carolina in order to make data integration at a future point in time a possibility.  The data would be available to people on an approved to receive datalist since the data need to remain private to thwart poachers.  During the nesting season, the plan is to make the data available every day via a text file that could be viewed in Excel, and a .kml file that can be viewed in Google Earth.  By getting a new file each day, approved individuals can see the data that was added the day before as well as all the data collected up until that point. An immediate benefit of the tool would be to make it easy for volunteers and conservation staff to collect data and visualize the results of their efforts.  The ultimate goal is to further scientific research by providing researchers with a standardized database of nesting and stranding data from which they can gain insights into sea turtle biology. ", 
 hostname: "colab-sbx-05.oit.duke.edu", info: " I would like to provide a secure (SSL?) web page/folder/ftp site where people who I have told about the web page/folder/ftp site can download the .csv and .kml files.  Since I am still learning about server-side technologies, I'm not sure how to accomplish this (I'm thinking maybe by using Apache commands...?), but wanted to make sure that this capability would be available.) If it is possible to use Python on this server, that would allow me to do some GIS-type things with the data collected by the app.) At some point in the future it might be a good idea to migrate the database over to PostgreSQL to be compatible with other databases maintained by the Marine Geospatial Ecology Lab, however this is a very low priority at this point in time.", 
 shortname: "colab-sbx-05.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")


nr =NetworkResource.create(:dns=>'colab-sbx-06.oit.duke.edu', :ip=>'152.3.53.12',:mac_addr=>'00:50:56:bb:66:88')
uid = User.create(:duid => "0586770", :netid => "jh365", :displayName=> "Jeff Hou", :phone=> "+1 919 433 7388", 
 :email=> "jiefu.hou@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-ruby').id, 
 admin_password: "Trathsibpo")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "jh365", technical_contact_name: "Jeff Hou",
  other_contacts: "", 
  proj_name: "Logarithmic Calendar", tandc: true, image_name: "vcl-bitnami-ruby", 
  description: "This will be an online calendar that will allow users to look at events on a logarithmic scale. ", 
  hostname: "colab-sbx-06.oit.duke.edu", info: "", 
  shortname: "colab-sbx-06.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")
  
nr = NetworkResource.create(:dns=>'colab-sbx-07.oit.duke.edu', :ip=>'152.3.53.13',:mac_addr=>'00:50:56:bb:6E:0A')
uid = User.create(:duid => "0560187", :netid => "jas138", :displayName=> "Jonathan Schmidt", :phone=> "+1 720 256 4696", 
  :email=> "jas138@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-lamp').id, admin_password: "wabastuxim")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "jas138", technical_contact_name: "Jonathan Schmidt",
 other_contacts: "Kevin Oh - oh.justinian@gmail.com / A few others.", 
 proj_name: "Gotta.do", tandc: true, image_name: "vcl-bitnami-lamp", 
 description: "This will be an advanced task / to-do list that keeps track of what you need to do, your daily schedule and tells you when the best time would be to work on the things you need to do.  / It would be catered towards students first, so take in their class schedule and then when they add what homework / assignments they need to complete it will basically tell them when to work on it in order to complete it. / User would estimate time to complete it and it would then be able to allocate the correct time appropriately.", 
 hostname: "colab-sbx-07.oit.duke.edu", info: "", 
 shortname: "colab-sbx-07.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
  vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")
  
nr= NetworkResource.create(:dns=>'colab-sbx-08.oit.duke.edu', :ip=>'152.3.53.16',:mac_addr=>'00:50:56:bb:0D:F7')
uid = User.create(:duid => "0561503", :netid => "yr18", :displayName=> "Yao Rong", :phone=> "+1 919 699 6738", 
  :email=> "yao.rong@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-lamp').id, admin_password: "rihecackye")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "yr18", technical_contact_name: "Yao Rong",
 other_contacts: "Professor Andrew Hilton adhilton@ee.duke.edu / Professor Chris Dwyer dwyer@ece.duke.edu", 
 proj_name: "Duke ECE graduate central panel", tandc: true, image_name: "vcl-bitnami-lamp", 
 description: "Duke ECE graduate central panel is a central panel for graduate students, professors and staff.", 
 hostname: "colab-sbx-08.oit.duke.edu", info: "", 
 shortname: "colab-sbx-08.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
  vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")

nr = NetworkResource.create(:dns=>'colab-sbx-09.oit.duke.edu', :ip=>'152.3.53.15',:mac_addr=>'00:50:56:bb:51:30')
uid = User.create(:duid => "0493679", :netid => "aer19", :displayName=> "Aaron Rales", :phone=> "+1 301 318 3589", 
  :email=> "aaron.rales@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-wordpress').id, admin_password: "viekonkek5")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "aer19", technical_contact_name: "Aaron Rales",
 other_contacts: "Ross Tucker - net id: ret11", 
 proj_name: "DukeSocial.com", tandc: true, image_name: "vcl-bitnami-wordpress", 
 description: "DukeSocial is a social media and editorial blog that is designed to help Duke students discover, share, and connect with relevant opportunities and resources on campus.", 
 hostname: "colab-sbx-09.oit.duke.edu", info: "", 
 shortname: "colab-sbx-09.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
  vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")
  
nr= NetworkResource.create(:dns=>'colab-sbx-10.oit.duke.edu', :ip=>'152.3.53.9',:mac_addr=>'00:50:56:bb:3E:B5')
uid = User.create(:duid => "0493649", :netid => "ret11", :displayName=> "Ross Tucker", :phone=> "+1 908 361 4807", 
  :email=> "rtucker511@gmail.com", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-wordpress').id, admin_password: "Jenpomyav.")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "ret11", technical_contact_name: "Ross Tucker",
 other_contacts: "Stephanie Mactas / Dylan Flye / Jed Lavery", 
 proj_name: "DashTogether - Surprise Endings Final Project", tandc: true, image_name: "vcl-bitnami-wordpress", 
 description: "My classmates and I are making a website for our course, Surprise Endings, taught by Professors Dan Ariely and Cathy Davidson. We have purchased a wordpress theme and a domain name \"dashtogether.com\" and would like to host it using the co-lab servers. The basic bitnami application looks like it will be ideal for our needs", 
 hostname: "colab-sbx-10.oit.duke.edu", info: "", 
 shortname: "colab-sbx-10.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")

nr = NetworkResource.create(:dns=>'colab-sbx-11.oit.duke.edu', :ip=>'152.3.53.5',:mac_addr=>'00:50:56:bb:71:BA')
uid = User.create(:duid => "0220198", :netid => "tsalazar", :displayName=> "Teddy Salazar", :phone=> "+1 919 681 3585", 
  :email=> "tsalazar@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-ruby').id, admin_password: "icMilIbjuc.")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "tsalazar", technical_contact_name: "Teddy Salazar",
 other_contacts: "", 
 proj_name: "get shibbed", tandc: true, image_name: "vcl-bitnami-ruby", 
 description: "Test shibboleth installation in ubuntu", 
 hostname: "colab-sbx-11.oit.duke.edu", info: "", 
 shortname: "colab-sbx-11.oit.duke.edu", ip: nil, step: 5, created_at: "2013-04-18 21:45:37")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-04-18 21:45:37")
 
nr = NetworkResource.create(:dns=>'colab-sbx-12.oit.duke.edu', :ip=>'152.3.53.11',:mac_addr=>'00:50:56:b1:7a:4b')
uid = User.create(:duid => "0489988", :netid => "liz", :displayName=> "Liz Wendland", :phone=> "+1 919 613 6218", 
  :email=> "liz@duke.edu", :vms_allowed=> 10)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-node').id, admin_password: "vapawckyee.")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "liz", technical_contact_name: "Liz Wendland",
 other_contacts: "", 
 proj_name: "Test Node", tandc: true, image_name: "vcl-bitnami-node", 
 description: "Test instance for VM Manage", 
 hostname: "colab-sbx-12.oit.duke.edu", info: "", 
 shortname: "colab-sbx-12.oit.duke.edu", ip: nil, step: 5, created_at: "2013-06-05")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-06-05")

nr = NetworkResource.create(:dns=>'colab-sbx-13.oit.duke.edu', :ip=>'152.3.53.14',:mac_addr=>'00:50:56:B1:31:87')
uid = User.create(:duid => "0530164", :netid => "aks35", :displayName=> "Andrew Shim", :phone=> "+1 510 421 6295", 
  :email=> "aks35@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-ruby').id, admin_password: "eel3udee")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "aks35", technical_contact_name: "Andrew Shim",
 other_contacts: "", 
 proj_name: "Blue Devil Latin", tandc: true, image_name: "vcl-bitnami-ruby", 
 description: "Simple web application that is to assist students with studying for Latin.", 
 hostname: "colab-sbx-13.oit.duke.edu", info: "", 
 shortname: "colab-sbx-13.oit.duke.edu", ip: nil, step: 5, created_at: "2013-05-10")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-05-10")

nr = NetworkResource.create(:dns=>'colab-sbx-14.oit.duke.edu', :ip=>'152.3.53.17',:mac_addr=>'00:50:56:B1:5B:53')
uid = User.create(:duid => "0548780", :netid => "rsa18", :displayName=> "Robert Ansel", :phone=> "+1 361 816 6354", 
  :email=> "robert.ansel@gmail.com", :vms_allowed=> 3)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-lamp').id, admin_password: "rolfengOqu")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "rsa18", technical_contact_name: "Robert Ansel",
 other_contacts: "", 
 proj_name: "Colab Dev/Testing Server", tandc: true, image_name: "vcl-bitnami-lamp", 
 description: "writing docs for the Colab and need a clean machine to work with", 
 hostname: "colab-sbx-14.oit.duke.edu", info: "", 
 shortname: "colab-sbx-14.oit.duke.edu", ip: nil, step: 5, created_at: "2013-06-18")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-06-18")
 
nr = NetworkResource.create(:dns=>'colab-sbx-15.oit.duke.edu', :ip=>'152.3.53.18',:mac_addr=>'00:50:56:B1:43:3E')
uid = User.find_by_netid("dcg13")
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-lamp').id, admin_password: "aethees9")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "dcg13", technical_contact_name: "Davis Gossage",
 other_contacts: "", 
 proj_name: "Transloc Duke Mobile Module", tandc: true, image_name: "vcl-bitnami-lamp", 
 description: "Integrate Transloc with Duke Mobile for iOS to allow personalized ETAs, routes, etc. based on schedule and current location.", 
 hostname: "colab-sbx-15.oit.duke.edu", info: "", 
 shortname: "colab-sbx-15.oit.duke.edu", ip: nil, step: 5, created_at: "2013-06-07")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-06-07")

nr =  NetworkResource.create(:dns=>'colab-sbx-16.oit.duke.edu', :ip=>'152.3.53.19',:mac_addr=>'00:50:56:B1:09:80')
uid = User.create(:duid => "0583627", :netid => "mk234", :displayName=> "Manoj Kanagaraj", :phone=> "+1 909 631 0943", :email=> "mk234@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-ruby').id, admin_password: "hyrobrIdIf")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "rsz3", technical_contact_name: "Roger Zou",
 other_contacts: "mk234@duke.edu", 
 proj_name: "Duke Office Hours", tandc: true, image_name: "vcl-bitnami-ruby", 
 description: "Our web app is designed for the Co-Lab Duke Mobile Challenge, and serves to provide both Duke students and staff with a resource to easily access and update campus-wide office hours.",
 hostname: "colab-sbx-16.oit.duke.edu", info: "", 
 shortname: "colab-sbx-16.oit.duke.edu", ip: nil, step: 5, created_at: "2013-06-24")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, vm_request_id: vmReq.id, start_date: "2013-06-24")


nr = NetworkResource.create(:dns=>'colab-sbx-17.oit.duke.edu', :ip=>'152.3.53.20',:mac_addr=>'00:50:56:B1:69:E3')
uid = User.create(:duid => "0586108", :netid => "eas66", :displayName=> "Emmanuel Shiferaw", :phone=> "+1 919 961 4590", 
  :email=> "eas66@duke.edu", :vms_allowed=> 1)
vm = Vm.create(network_resource_id: nr.id, image_id: Image.find_by_name('vcl-bitnami-django').id, admin_password: "phaisJublo")
vmReq = VmRequest.create(user_id: uid.id, technical_contact_netid: "eas66", technical_contact_name: "Emmanuel Shiferaw",
 other_contacts: "", 
 proj_name: "CollabDuke", tandc: true, image_name: "vcl-bitnami-django", 
 description: "Group/Project Collaboration service", 
 hostname: "colab-sbx-17.oit.duke.edu", info: "", 
 shortname: "colab-sbx-17.oit.duke.edu", ip: nil, step: 5, created_at: "2013-07-05")
res = Reservation.create(reservation_state_id: ReservationState.find_by_name('Reserved').id, vm_id: vm.id, 
 vm_request_id: vmReq.id, start_date: "2013-07-05")
  
  
  NetworkResource.create(:dns=>'colab-sbx-18.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-19.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-20.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-21.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-22.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-23.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-24.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-25.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-26.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-27.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-28.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-29.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-30.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-31.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-32.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-33.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-34.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-35.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-36.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-37.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-38.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-39.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-40.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-41.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-42.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-43.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-44.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-45.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-46.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-47.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-48.oit.duke.edu')
  NetworkResource.create(:dns=>'colab-sbx-49.oit.duke.edu')

  (0..75).each do |i|
    NetworkResource.create(:dns=> sprintf("colab-sbx-2%02d.oit.duke.edu",i))
  end

  (0..30).each do |i| 
    NetworkResource.create(dns: sprintf("colab-sbx-pvt-%02d.oit.duke.edu",i), network_type: 'private')
  end

Admin.delete_all
Admin.create(:displayName => "Liz Wendland", :netid => "liz", :dukeId => '0489988')


Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'30000', :pw=>'badpassword', :appname=>'RStudio', :appdesc=> 'statistics application with Rmarkdown and knitr support')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'30000', :pw=>'badpassword', :appname=>'NCCU-RStudio', :appdesc=> 'statistics application with Rmarkdown and knitr support')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'30000', :pw=>'badpassword', :appname=>'Davidson-CSC-110-0', :appdesc=> 'Davidson RStudio for Data Science & Society')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'30000', :pw=>'badpassword', :appname=>'Davidson-POL-182-A', :appdesc=> 'Davidson RStudio for Introduction to Political Science Methods')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'40000', :pw=>'badpassword', :appname=>'Jupyter', :appdesc=> 'interactive data science and scientific computing notebooks')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'40000', :pw=>'badpassword', :appname=>'PyROOT', :appdesc=> 'Jupyter with CERN ROOT analytic package')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'40000', :pw=>'badpassword', :appname=>'NCCU-Jupyter', :appdesc=> 'interactive data science and scientific computing notebooks')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'40000', :pw=>'badpassword', :appname=>'Davidson-MAT-315-0', :appdesc=> 'Davidson JupyterLab for Numerical Analysis')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'50000', :pw=>'badpassword', :appname=>'Pytorch', :appdesc=> 'machine learning notebooks in Jupyterlab')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'50000', :pw=>'badpassword', :appname=>'NCCU-Pytorch', :appdesc=> 'machine learning notebooks in Jupyterlab')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'50000', :pw=>'badpassword', :appname=>'Davidson-VSCode', :appdesc=> 'VSCode for Davidson CSC250')
Docker.create(:netid=> '', :host=>'labs-ncs-00.ncshare.org', :port=>'30000', :pw=>'badpassword', :appname=>'Knime', :appdesc=> 'Data analytics platform')
