class DropNewImagesTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :new_images 
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
