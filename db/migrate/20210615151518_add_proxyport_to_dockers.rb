class AddProxyportToDockers < ActiveRecord::Migration[6.0]
  def change
    add_column :dockers, :proxyport, :integer
  end
end
