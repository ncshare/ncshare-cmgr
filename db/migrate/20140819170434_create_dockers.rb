class CreateDockers < ActiveRecord::Migration
  def change
    create_table :dockers do |t|
      t.string :netid
      t.text :host
      t.integer :port
      t.string :pw
      t.string :description
      t.timestamps
    end
  end
end
