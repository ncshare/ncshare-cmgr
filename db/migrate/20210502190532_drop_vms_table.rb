class DropVmsTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :vms
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
