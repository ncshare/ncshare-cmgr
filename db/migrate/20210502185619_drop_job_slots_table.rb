class DropJobSlotsTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :job_slots 
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
