class DropGroupsImagesTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :groups_images
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
