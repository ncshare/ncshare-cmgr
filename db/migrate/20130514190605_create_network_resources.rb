class CreateNetworkResources < ActiveRecord::Migration
  def change
    create_table :network_resources do |t|
      t.string :ip
      t.string :mac_addr
      t.string :dns

      t.timestamps
    end
  end
end
