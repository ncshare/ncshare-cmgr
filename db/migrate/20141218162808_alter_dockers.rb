class AlterDockers < ActiveRecord::Migration
  def up
    rename_column :dockers, :description, :appname
    add_column :dockers, :appdesc, :text
    add_column :dockers, :expired, :boolean, default: false
  end

  def down
    rename_column :dockers, :appname, :description  
    remove_column :dockers, :expired
    remove_column :dockers, :appdesc   
  end
end
