class DropVmRequestsTable < ActiveRecord::Migration[6.0]
 def up
    drop_table :vm_requests
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
