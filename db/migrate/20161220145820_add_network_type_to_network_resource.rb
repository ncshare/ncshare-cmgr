class AddNetworkTypeToNetworkResource < ActiveRecord::Migration
  def change
    add_column :network_resources, :network_type, :string, default: 'public'
  end
end
