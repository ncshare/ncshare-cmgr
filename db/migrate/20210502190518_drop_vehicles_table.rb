class DropVehiclesTable < ActiveRecord::Migration[6.0]
 def up
    drop_table :vehicles
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
