class RemoveLastGroupRefreshFromUsers < ActiveRecord::Migration[6.0]
  def change
    remove_column :users, :last_group_refresh, :datetime
  end
end
