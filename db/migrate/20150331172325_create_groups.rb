class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :urn

      t.timestamps
    end
    
    # Create the group we were actually using up to this point so we can assign it to all current images
    ActiveRecord::Base.connection.execute("INSERT INTO groups (id, urn, created_at, updated_at) " +
      "VALUES (1, 'duke', NOW(), NOW())")      
  end
end
