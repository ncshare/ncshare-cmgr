class CreateNewImages < ActiveRecord::Migration
  def change
    create_table :new_images do |t|
      t.string :zipfile
      t.string :name
      t.integer :user_id
      t.integer :image_id
      t.string :status

      t.timestamps
    end
  end
end
