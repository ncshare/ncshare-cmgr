class AddNetworkTypeToImage < ActiveRecord::Migration
  def change
    add_column :images, :network_type, :string, default: 'public'
  end
end
