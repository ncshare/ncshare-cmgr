class CreatePosixUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :posix_users do |t|
      t.string "institution"
      t.string "gitlab_user"
      t.string "gitlab_displayname"
      t.string "gitlab_public_key"
      t.string "shell_preference"
      t.string "ldap_entry"
      t.integer "user_id"
      t.boolean "posix_active", default: true
      t.timestamps
    end
  end
end
