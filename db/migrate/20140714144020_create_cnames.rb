class CreateCnames < ActiveRecord::Migration
  def change
    create_table :cnames do |t|
      t.string :alias
      t.boolean :renewed, default: true
      t.string :target
      t.string :status
      t.references :user

      t.timestamps
    end
  end
end
