class DropDelayedJobsTable < ActiveRecord::Migration[6.0]
 def up
    drop_table :delayed_jobs
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
