class DropCnamesTable < ActiveRecord::Migration[6.0]
 def up
    drop_table :cnames
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
