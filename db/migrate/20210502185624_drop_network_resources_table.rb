class DropNetworkResourcesTable < ActiveRecord::Migration[6.0]
  def up
    drop_table :network_resources
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
