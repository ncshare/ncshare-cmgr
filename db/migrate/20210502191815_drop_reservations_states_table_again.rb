class DropReservationsStatesTableAgain < ActiveRecord::Migration[6.0]
  def up
    drop_table :reservation_states
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
