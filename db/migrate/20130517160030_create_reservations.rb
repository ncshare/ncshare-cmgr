class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.integer :reservation_state_id
      t.integer :vm_id
      t.integer :vm_request_id
      t.date :start_date
      t.date :end_date
      t.timestamps
    end
  end
end
