class AlterPosixUsers < ActiveRecord::Migration[6.0]
  def up
    change_column(:posix_users, :gitlab_public_key, :text)
  end

  def down
    change_column(:posix_users, :gitlab_public_key, :string)
  end
end
