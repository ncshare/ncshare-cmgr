class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :folder
      t.string :name
      t.string :pretty_name
      t.text :desc

      t.timestamps
    end
  end
end
