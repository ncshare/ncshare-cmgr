# Load the Rails application.
require_relative 'application'
myenv = ENV.key?('LOCATION') ? "#{ENV['LOCATION']}_#{Rails.env}" : Rails.env
APP_CONFIG = YAML.load_file(Rails.root.join('config', 'cmgr-vmm.yml'))[myenv]

# Initialize the Rails application.
Rails.application.initialize!
