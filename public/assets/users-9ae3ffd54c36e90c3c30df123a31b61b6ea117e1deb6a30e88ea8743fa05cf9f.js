$(document).on('turbolinks:load', function() {
  if (!($('#ssh_key').length > 0)) {
    console.log("ignoring for non key page");
    return;
  }
  console.log("doing it for key page");

  validate();
  $('#ssh_key, #ssh_key_title').change(validate);
});

function validate(){
  console.log("hey inside validate");
  if ($('#ssh_key').val().length   >   0   &&
      $('#ssh_key_title').val().length  >   0) {
    console.log("hey enable");
    $("#create-ssh-key-btn").prop("disabled", false);
  } else {
    console.log("hey disenable");
    $("#create-ssh-key-btn").prop("disabled", true);
  }
};
