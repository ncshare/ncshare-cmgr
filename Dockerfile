FROM docker.io/ruby:2.6.5
MAINTAINER Mark McCahill <mccahill@duke.edu>

# Update system and Install extra packages
RUN apt-get update && apt-get dist-upgrade -y && \
    apt-get install -y --no-install-recommends \
	  apt-utils \
      openssl \
	  bash \
	  libxml2-dev \
	  libxslt-dev \
	  mariadb-client \
	  curl \
	  less \
	  vim \
	  wget \
	  coreutils && \
    # We need a newer version of node / yarn for webpacker
    curl -sL https://deb.nodesource.com/setup_10.x | bash - &&\
    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    apt-get update && apt-get install -y \
	  nodejs \
	  yarn && \
    rm -rf /var/lib/apt/lists/*

ENV TZ America/New_York

# Set up our docker user
RUN echo 'docker-user:x:1000:1000:docker container user:/home/docker-user:/bin/bash' >> /etc/passwd
RUN echo 'docker-user:x:1000:' >> /etc/group
RUN mkdir -p ~docker-user && chown -R docker-user ~docker-user

RUN mkdir /app
RUN chown docker-user:docker-user /app
USER  docker-user
COPY Gemfile /app/
COPY ssiapp-layout /app/
COPY Gemfile.lock /app/
WORKDIR /app

RUN gem install bundler:1.17.3
ENV NOKOGIRI_USE_SYSTEM_LIBRARIES=1
RUN echo "gem: --no-document" > /home/docker-user/.gemrc

# Now we get everything else in
ADD .  /app

# Set up some permissions so we can run without root
#RUN chmod -Rf g+rwx tmp && chgrp -R 1000 tmp && \
#    chmod -Rf g+rwx db && chgrp -R 1000 db &&\
#    chmod -Rf g+rwx log && chgrp -R 1000 log &&\
#    chmod -Rf g+rwx config/saml/* && chgrp -R 1000 config/saml/* &&\
#    chmod -Rf g+rwx /etc/passwd && chgrp -R 1000 /etc/passwd 
#
USER root
RUN chmod -Rf g+rwx /usr/local/bundle && chgrp -R 1000 /usr/local/bundle
RUN chmod -Rf g+rwx /app && chgrp -R 1000 /app

USER docker-user
RUN bundle install

# can't precompile assests without the database - do this in the run-app script
#RUN bundle exec rake assets:precompile --trace
#RUN RAILS_ENV=production bundle exec rake assets:precompile

CMD ["/app/bin/app-start"]
