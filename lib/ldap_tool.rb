# ldap management
class LdapTool
  TREEBASE = 'OU=people,DC=duke,DC=edu'.freeze

  def self.userLookup(searchStr)
    return nil if searchStr.blank?

    a = Net::LDAP::Filter.eq('edupersonaffiliation', 'student')
    b = Net::LDAP::Filter.eq('edupersonaffiliation', 'staff')
    c = Net::LDAP::Filter.eq('edupersonaffiliation', 'faculty')
    d = Net::LDAP::Filter.eq('edupersonaffiliation', 'emeritus')
    e = Net::LDAP::Filter.eq('edupersonaffiliation', 'affiliate')

    if !/ /.match(searchStr) # Single word case
      searchStr.concat '*'
      j = Net::LDAP::Filter.eq('uid', searchStr)
      k  = Net::LDAP::Filter.eq('givenname', searchStr)
      l  = Net::LDAP::Filter.eq('cn', searchStr)
      m  = Net::LDAP::Filter.eq('sn', searchStr)
      op_filter = (k | l | j | m) # match netid firstname or nickname and their last name
    else
      words = searchStr.split(' ')
      words.each do |word|
        word.concat('*') if /\*/.match(word).nil?
      end
      k  = Net::LDAP::Filter.eq('givenname', words[0])
      l  = Net::LDAP::Filter.eq('cn', "#{words[0]} #{words[1]}")
      op_filter = (k | l)
      op_filter = (op_filter & Net::LDAP::Filter.eq('sn', words[words.size - 1])) if words.size > 1
    end

    op_filter = (a | b | c | d | e) & op_filter
    ret_array = {}
    Rails.logger.info("search is: #{op_filter}")
    ldap_con.search(base: LdapTool::TREEBASE, filter: op_filter, size: 15, return_result: false) do |entry|
      if entry.attribute_names.include?(:uid)
        name = entry.attribute_names.include?(:displayname) ? "#{entry.displayname.first} (#{entry.uid.first})" : entry.uid.first
        ret_array[entry.uid.first] = name
      end
    end
    ret_array
  end

  def self.createUser(netid)
    attrs = %w[displayName uid mail uidnumber]
    op_filter = Net::LDAP::Filter.construct("uid=#{netid}")
    ldap_array = ldap_con.search(base: LdapTool::TREEBASE, filter: op_filter, attrs: attrs, return_result: true)
    return nil if ldap_array.blank? || ldap_array.size > 1

    anames = ldap_array.first.attribute_names
    u = User.find_or_initialize_by(netid: netid)
    u.netid = anames.include?(:uid) ? ldap_array.first.uid.first : nil
    u.display_name = anames.include?(:displayname) ? ldap_array.first.displayname.first : 'Missing'
    u.email_address = anames.include?(:mail) ? ldap_array.first.mail.first : 'Missing'
    u.uidnumber = anames.include?(:uidnumber) ? ldap_array.first.uidnumber.first : nil
    u.save
    u
  end

  def self.get_ldap_info(duid_list)
    return [] if duid_list.empty?
    ldap_query = "(|#{duid_list.collect { |d| "(duDukeID=#{d})" }.join})"
    # use a wider treebase to allow service accounts
    ldap_con.search(
      base: 'DC=duke,DC=edu',
      filter: Net::LDAP::Filter.construct(ldap_query),
      attributes: %w[uid],
      return_result: true
    ).collect { |u| u[:uid].first }
  end

  def self.ldap_con
    Net::LDAP.new(host: 'ldap.duke.edu',
                  port: 636,
                  encryption: :simple_tls,
                  auth: { method: :anonymous })
  end
end
