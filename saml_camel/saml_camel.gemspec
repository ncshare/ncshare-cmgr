# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'saml_camel/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'saml_camel'
  s.version     = SamlCamel::VERSION
  s.authors     = ['Danai Adkisson ']
  s.email       = ['da129@duke.edu']
  s.homepage    = 'https://gitlab.oit.duke.edu/da129/saml_camel'
  s.summary     = 'SAML tool wrapping onelogin/rubysaml'
  s.description = 'SAML tool wrapping onelogin/rubysaml'
  s.license     = 'MIT'
  s.metadata = {
    'documentation_uri' => 'https://gitlab.oit.duke.edu/da129/saml_camel/blob/master/README.md',
    'homepage_uri' => 'https://gitlab.oit.duke.edu/da129/saml_camel',
    'changelog_uri' => 'https://gitlab.oit.duke.edu/da129/saml_camel/blob/master/CHANGELOG.MD'
  }
  s.required_ruby_version = '>= 2.3.0'

  s.files = Dir['{app,config,db,lib}/**/*',
                'MIT-LICENSE',
                'Rakefile',
                'README.md'
               ]

  s.add_dependency 'rails', '>= 4.0.0'
  s.add_dependency 'ruby-saml', '>= 1.7.2'

  s.add_development_dependency 'byebug'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'ruby-saml', '>= 1.7.2'
end
