# CHANGELOG
# 1.0.8
* Fixnum has been deprected and class coming through clock drift has not been consistant, now checking that option with a regular expression.

# 1.0.7
* attribute map will now simply point to the settings.json configuration for the environment as opposed
to having a separate shibboleth.json file. The shibboleth.json will still work to maintain backwards compatibility.
* now able to set relay_state an an optional named parameter in saml protect
* add option to set clock drift
* add ids to html elements on error pages to allow easier customization
* fix bug where user changing IP mid sessions would get an error screen after successful auth with idp

# 1.0.6 (had an issue with versions hence the jump)
* fixed issue where it appears that some systems were returning sp_session as a time object and others a string.

# 1.0.3
* added debug log lines to better see why a saml response is being generated

# 1.0.2
* put a configurable toggle on whether or not to load the shibboleth.json file for the shib module. This was causing things to break in a production environment.

# 1.0.1
* add url to generate metadata
* add url to view decrypted saml response
* add DEBUG flag to provide more in depth logs
* several syntax adjustments to be in line with ruby style guides
* add testing for sp logic
* add a class method to mock a cache for testing

# 1.0.0
* initial production release

## 0.2.6 pre-release
* saml settings now under config to keep with rails conventions
* fixed potential issue where not calling `.to_sym` when fetching from cache
* moved gem versions to pkg directly
* added additional notes and linked to docs in the readme in regards to caching
* namespace cache permit key
* loading settings as SAML_CAMEL basicConstraints
* loading keys and certs as constant
* fix potential race condition when storing response ids by locking cache when in use
* move transaction and logging into modules
* fixed bug where logging option was being ignored


## 0.2.4 pre-release
* fix bug shib attributes

## 0.2.4 pre-release
* fix bug undefined var

## 0.2.3 pre-release
* add model to get Shibboleth attributes set by apache

## 0.2.2 pre-release
* match? to match in sha1 check for more compatibility

## 0.2.1 pre-release
* check ip address on every request instead of just consumes
* give options for sp expiration (lifetime and timeout)
* cache now persists to store ip address for checking
* fix bug from corrupt git merge causing saml session to not properly get removed
* store saml response id so that same saml response can not be used twice


## 0.2.0 pre-release
* add error if cache can not be accessed
* change rake text format to make more obvious asking for user input

## 0.1.9 pre-release
* add backtrace for saml consume errors

## 0.1.8 pre-release
* fix bug in rescue from consume where saml_session_id was already nil when rescued

## 0.1.7 pre-release
* several fixes to README.md
* fixed bug where the relay state was being mal-formed in the response from IDP
* fixed bug with session_id not being present, now setting saml_session_id

## 0.1.6 pre-release
* no longer require the includes in application controller to work

## 0.1.5 pre-release
* verify client IP address on SAML response from idp
* set 1 hour idle timeout for SP session
* move saml security check validations to memory vs encrypted cookie

## 0.1.4 pre-release
 * append lines to ignore private keys to `.gitignore` file. Creates file if no file.
 * add error redirect screen
 * rescue unknown logging errors
 * fixed but if anonymous user goes to logout link
 * add RelayState nonce to protect against replay attack

## 0.1.3 pre-release
 * fixed logging bug
 * can now use before_action for saml_protect
 * secured redirect cookie

## 0.1.2 pre-release
 * added a `/attributes` path for a user to check attributes coming through
 * removed abstract class in application record, may have been causing an error in prod environments.
 * added logging for saml auth, logouts, and SAML errors

## 0.1.1 pre-release
 * added logout functionality
 * the SAML call no longer makes a call to the IDP before every "saml_protect" request
 * modified rake task to account for logout


## 0.1.0 - initial pre-release
