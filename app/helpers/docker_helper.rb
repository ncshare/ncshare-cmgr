module DockerHelper

  def docker_port_column(record, column)
    if record.port
      record.port.to_s
    else
      # This way there's something to turn into a link if there are no ports associated with this record yet.
      active_scaffold_config.list.empty_field_text
    end
  end


  def docker_proxyport_column(record, column)
    if record.proxyport
      record.proxyport.to_s
    else
      active_scaffold_config.list.empty_field_text
    end
  end


end
