# Various helpers
module InformationHelper
  def title_str
    'Container Manager Information'
  end

  def logo
    'ssiapp/logo.png'
  end

  def application_name
    if Rails.env.production?
      'Container Manager'
    else
      "Container Manager: <span style='font-size:smaller'>#{Rails.env}</span>".html_safe
    end
  end


  def secondary_nav_links
    links = []
    # if Rails.env.production?
      if @current_user
        links << "<span style='color:white'>Welcome, #{@current_user.displayName}.</span>".html_safe unless @current_user.displayName.blank?
        links << link_to('Log out', '/home/logout',  method: :post)
      else
        #links << link_to('Login via InCommon', '/incommon_login/home/index')
      end
      # end
    links
  end

  def nav_right
  end

  def nav_links
    if (params[:controller] == 'information')
      login_menu = "<a class='dropdown-toggle' data-toggle='dropdown' href='#'>Select Login<span class='caret'></span></a>
      <ul class='dropdown-menu'>
        <li>#{ link_to('InCommon', '/incommon_login/home/index')}</li>
        <li>#{ link_to('Davidson', '/davidson_login/home/index')}</a></li>
        <li>#{ link_to('Duke ', '/duke_login/home/index')}</a></li>
        <li>#{ link_to('Elon ', '/elon_login/home/index')}</a></li>
        <li>#{ link_to('NCCU', '/nccu_login/home/index')}</a></li>
        <li>#{ link_to('UNCW', '/uncw_login/home/index')}</a></li>
      </ul>"
      links = [{ class: 'active', link: login_menu }]
    else
      current_item_home = params[:controller] == 'home' ||
                          (params[:controller] == 'containers' && !params[:id].blank?)
      links = [{ class: current_item_home && 'active', link: link_to('Home', home_index_path) }]
      if @current_user
        links += [
          {
            class: params[:controller] == 'containers' && !current_item_home && 'active',
            link: link_to('Reserve a Container', containers_path)
          }
        ]
        links += [
          {
            class: params[:controller] == 'containers' && !current_item_home ,
            link: link_to('SLURM/OnDemand', '/posix/index')
          }
        ]

        if Admin.is_admin(@current_user.netid)
          admin_menu = "<a class='dropdown-toggle' data-toggle='dropdown' href='#'>Admin
          <span class='caret'></span></a>
          <ul class='dropdown-menu'>
            <li>#{link_to('Activities', activity_logs_path)}</li>
            <li>#{link_to('Users', users_path)}</a></li>
            <li>#{link_to('Posix Users', posix_users_path)}</a></li>
            <li>#{link_to('Containers', dockers_path)}</a></li>
            <li>#{link_to('Admins', admins_path)}</a></li>
            <li>#{link_to('History', hourly_activity_path)}</a></li>
            <li>#{link_to('Utilization', container_utilization_path)}</a></li>
          </ul>"
          links += [  {
            class: (request.env['PATH_INFO'].match(/^\/admin/) && 'active') || 'dropdown',
            link: admin_menu
            } ]
        else
          links
        end
      end
    end
#    links << {
#      class: params[:controller] == 'help' && 'active',
#      link: link_to('Help', help_path)
#    }
  end


  def markdown(text)
    options = {
      filter_html:     true,
      hard_wrap:       true,
      link_attributes: { rel: 'nofollow', target: '_blank' },
      prettify:	       true,
      space_after_headers: true,
      fenced_code_blocks: true,
      with_toc_data: true
    }

    extensions = {
      autolink:           true,
      fenced_code_blocks: true,
      no_intra_emphasis:  true,
      superscript:        true
    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end

end
