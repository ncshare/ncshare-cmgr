# Various helpers
module ApplicationHelper
  def title_str
    if Rails.env.production?
      'Container Manager'
    else
      "VCM: #{Rails.env}"
    end
  end

  def logo
      'ssiapp/logo.png'
  end

  def application_name
    if Rails.env.production?
      'Container Manager'
    else
      "Container Manager: <span style='font-size:smaller'>#{Rails.env}</span>".html_safe
    end
  end


  def secondary_nav_links
    links = []
    # if Rails.env.production?
      if @current_user
        links << "<span style='color:white'>Welcome, #{@current_user.displayName}.</span>".html_safe unless @current_user.displayName.blank?
        links << link_to('Log out', '/home/logout',  method: :post)
      else
        links << link_to('Login via InCommon', '/incommon_login/home/index')
      end
      # end
    links
  end

  def nav_right
   # render(partial: '/home/search_bar')
  end

  def nav_links
    if (params[:controller] == 'information')
      links = [{ class: FALSE, link: link_to('Login', '/incommon_login/home/index') }]
    else
      current_item_home = params[:controller] == 'home' ||
                          (params[:controller] == 'containers' && !params[:id].blank?)
      links = [{ class: current_item_home && 'active', link: link_to('Home', home_index_path) }]
      if @current_user
        links += [
          {
            class: params[:controller] == 'containers' && !current_item_home && 'active',
            link: link_to('Reserve a Container', containers_path)
          }
        ]
        links += [
          {
            class: params[:controller] == 'containers' && !current_item_home ,
            link: link_to('SLURM/OnDemand', '/posix/index')
          }
        ]


        if  Admin.is_admin(@current_user.netid)
          admin_menu = "<a class='dropdown-toggle' data-toggle='dropdown' href='#'>Admin
          <span class='caret'></span></a>
          <ul class='dropdown-menu'>
            <li>#{link_to('Activities', activity_logs_path)}</li>
            <li>#{link_to('Users', users_path)}</a></li>
            <li>#{link_to('Posix Users', posix_users_path)}</a></li>
            <li>#{link_to('Containers', dockers_path)}</a></li>
            <li>#{link_to('Admins', admins_path)}</a></li>
            <li>#{link_to('History', hourly_activity_path)}</a></li>
            <li>#{link_to('Utilization', container_utilization_path)}</a></li>
          </ul>"
          links += [ {
            class: (request.env['PATH_INFO'].match(/^\/admin/) && 'active') || 'dropdown',
            link: admin_menu
            } ]
          else
            links
        end
      end
    end
#    links << {
#      class: params[:controller] == 'help' && 'active',
#      link: link_to('Help', help_path)
#    }
  end


  def format_time(time, cut_time = false)
    if time.respond_to? :to_formatted_s
      time = time.to_date if cut_time
      time.to_formatted_s :long
    else
      '&nbsp;'.html_safe
    end
  end

  def markdown(text)
    options = {
      filter_html:     true,
      hard_wrap:       true,
      link_attributes: { rel: 'nofollow', target: '_blank' },
      prettify:	       true,
      space_after_headers: true,
      fenced_code_blocks: true,
      with_toc_data: true
    }

    extensions = {
      autolink:           true,
      fenced_code_blocks: true,
      no_intra_emphasis:  true,
      superscript:        true
    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)

    markdown.render(text).html_safe
  end

  def pretty_print_gb(size)
    size ||= 0
    amount = size.abs * 1024 * 1024 * 1024
    txt = number_to_human_size(amount, significant: false, precision: 1)
    size.negative? ? "-#{txt}" : txt
  end
end
