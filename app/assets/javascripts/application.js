// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require active_scaffold

// Make table links clickable
function table_row_clickable(row, idx) {
  var undefined;
  var url;
  if (row._data != undefined && row._data.url != undefined) {
    url = row._data.url;
  } else if (row.url != undefined) {
    url = row.url;
  }
  if (url != undefined) {
    return {"onclick": "document.location = '" + url + "';"};
  }
}

$(document).on('turbolinks:load', function() {

  $('#table').on('check.bs.table uncheck.bs.table ' +
          'check-all.bs.table uncheck-all.bs.table', function () {
    $('#select-all').prop('disabled', !$(this).bootstrapTable('getSelections').length);
  });

  $('#table').on('expand-row.bs.table', function (e, index, row, $detail) {
    $detail.html('Loading from ajax request...');
    $.get({
      url: $(this).data('urlFrag')+row.id,
      data: {index: index},
      dataType: 'script',
      error: function(XMLHttpRequest, textStatus, errorThrown){
        debugger;
      }
    });
  });

  // close the dropdown menu
  $(".dropdown-menu a").click(function() {
      $(this).closest(".dropdown-menu").prev().dropdown("toggle");
  });

});
