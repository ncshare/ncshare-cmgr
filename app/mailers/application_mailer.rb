class ApplicationMailer < ActionMailer::Base
  default from: APP_CONFIG['contact_email']
  layout 'mailer'
end
