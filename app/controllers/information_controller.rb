class InformationController < ApplicationController
  


  #  before_action :login_required
  #  before_action :set_user  #If they have logged in
  #  before_action :onlyAdmin
  
    skip_before_action :saml_wayf_set
    skip_before_action :saml_protect
    skip_before_action :set_user
  
  # GET /information
  def index
  end

  #    "nccu.edu" => "https://idp.nccu.edu/idp/shibboleth",

  IDP_ENTITY_BY_INSTITUION = {
    "davidson.edu" => "https://sso.davidson.edu/idp",
    "duke.edu" => "urn:mace:incommon:duke.edu",
    "elon.edu" => "https://idp.elon.edu/idp/shibboleth",
    "nccu.edu" => "https://sts.windows.net/e86ab769-1eab-4e00-b79e-28ba7a8dbdf6/",
    "uncw.edu" => "https://sts.windows.net/22136781-9753-4c75-af28-68a078871ebf/"
  }

  def go_forth_and_auth
    logger.info("entityID is set to #{params[:entityID]}") 
    session[:wayf] = params[:entityID]
    SamlCamel::Transaction.set_instance_wayf(session[:wayf])

    # now that we have the IDP set, send them to their destination path
    path = params[:path]
    redirect_to "https://cmgr.ncshare.org/#{path}"
  end

  # GET /information/davidson_login/*path
  def davidson_login
    params[:entityID] = IDP_ENTITY_BY_INSTITUION["davidson.edu"]
    go_forth_and_auth
  end
  
  # GET /information/duke_login/*path
  def duke_login
    params[:entityID] = IDP_ENTITY_BY_INSTITUION["duke.edu"]
    go_forth_and_auth
  end
  
  # GET /information/elon_login/*path
  def elon_login
    params[:entityID] = IDP_ENTITY_BY_INSTITUION["elon.edu"]
    go_forth_and_auth
  end
  
  # GET /information/nccu_login/*path
  def nccu_login
    params[:entityID] = IDP_ENTITY_BY_INSTITUION["nccu.edu"]
    go_forth_and_auth
  end
  
  # GET /information/uncw_login/*path
  def uncw_login
    params[:entityID] = IDP_ENTITY_BY_INSTITUION["uncw.edu"]
    go_forth_and_auth
  end


  # GET /incommon_login/*path
  def incommon_login
    logger.info("***** incommon_login has #{request.original_url} *****")
    path = request.original_url.split('incommon_login')[1]
    logger.info("***** incommon_login has path #{path} *****")
    #path = params[:path]
    redirect_to "https://wayf.incommonfederation.org/DS/WAYF?entityID=https%3A%2F%2Fcmgr.ncshare.org%2F&return=https%3A%2F%2Fcmgr.ncshare.org%2Finformation%2Fuser_wayf%2F#{path}"
  end

  # GET /information/user_wayf/*path
  def user_wayf
    # whatever is tagged on to the path after /information/user_wayf is where the user will end up
    path = request.original_url.split('information/user_wayf')[1]
    logger.info("***** /information/user_wayf setting destination path to #{path} *****")
        
    # user_idp holds the IDP we should use for this session
    logger.info("entityID is set to #{params[:entityID]}") 
    session[:wayf] = params[:entityID]
    SamlCamel::Transaction.set_instance_wayf(session[:wayf])

    # now that we have the IDP set, send them to their destination path
    #redirect_to "https://cmgr.ncshare.org/#{path}"
    #
    # looks like we have to strip the entityID parameter in the URL returned from the InCommon WAYF service 
    # else this could interfere with the path we are appending for git-puller in Jupyter
    #
    path = path.split('&entityID=')[0]
    redirect_to "https://cmgr.ncshare.org/#{path}"
  end

end
