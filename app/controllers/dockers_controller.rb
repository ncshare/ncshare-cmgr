class DockersController < ApplicationController
  layout 'ssiapp/application'
  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin
  
  active_scaffold :"docker" do |conf|
    #conf.actions = [ :list, :search, :field_search]

    columns[:appname].weight = 0
    columns[:netid].weight = 1
    columns[:host].weight = 2
    columns[:port].weight = 3
    columns[:pw].weight = 4
    columns[:proxyhost].weight = 5
    columns[:proxyport].weight = 6
    columns[:appdesc].weight = 7
    columns[:expired].weight = 8
    columns[:updated_at].weight = 9
    conf.list.sorting = { :id => :asc }
    conf.list.per_page = 150
    config.columns = [ :appname, :netid, :host, :port, :pw, :proxyhost, :proxyport, :expired, :appdesc, :updated_at]
    #config.columns[:appname].label = 'application'
    #config.columns[:appdesc].label = 'description'
    #config.columns[:port].list_ui = :text
    list.columns.exclude :created_at
  end

  def expire_all
    sql = "select * from dockers where netid != '';"
    @containers_in_use = Docker.find_by_sql(sql)
    @containers_in_use.each do |item|
      item.expired = true
      item.save!
    end
    flash[:notice] = "All container reservations have been marked as expired"
    redirect_to action: :index
  end 
  
  def delete_expired
    sql = "select * from dockers where netid != '' and expired is TRUE;"
    @stale_containers = Docker.find_by_sql(sql)
    @stale_containers.each do |item|
      ActivityLog.log(session[:current_user_id], 'Container Provision',"Container flushed: #{item.netid}\t#{item.appname}\t#{item.host}\t#{item.port}")      
      item.netid = ''
      item.expired = false
      item.save!
    end   
    flash[:notice] = "All expired container reservations have been deleted"
    redirect_to action: :index
  end
  
  def expire_emails
    sql = "select distinct netid from dockers where expired is TRUE;"
    @expired_container_netids = Docker.find_by_sql(sql)
  end
   

end
