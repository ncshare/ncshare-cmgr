class PosixController < ApplicationController
  layout 'ssiapp/application'
  before_action :login_required
  before_action :set_user  #If they have logged in

require 'rest_client'
require 'net/ssh'
require 'net/ldap'

  def index
    @posix = PosixUser.new
    @posix[:gitlab_user] = ''
    @posix[:shell_preference] = 'bash'
  end

  def fetch_from_gitlab( gitlab_username )
    error = nil
    key = nil
    display_name = nil

    # we want valid linux username
    username_regex = /^[a-z][a-z0-9-]{0,30}$/
    if !username_regex.match?(gitlab_username) 
      error = "gitlab username '#{gitlab_username}' is not a valid linux user name"
    else  
      # see if we can fetch a public key for the user
      url = "https://gitlab.com/#{gitlab_username}.keys"
      begin
        response = RestClient.get(url)
      rescue RestClient::ExceptionWithResponse => e
        # error = e.response
        error = "cannot find gitlab.com account named '#{gitlab_username}'"
      end
      key = nil
      display_name = nil
      if error.nil? then
        begin 
        raw_key = response.body
          semi_cooked = raw_key.split(" (gitlab.com)\n")
          parts = semi_cooked[0].split(" ")
          key = "#{parts[0]} #{parts[1]}"
          display_name = semi_cooked[0].split("#{key} ")[1]
        rescue
          error = "cannot find public key for '#{gitlab_username}' at gitlab.com"
        end
      end
    end
    return({:error=>error, :key=>key, :display_name=>display_name})    
  end

  def next_id_for_array(min_id, idarray)
    for count in (min_id)..(min_id + 10000)
      if idarray.include?(count)
        count += 1
      else
        return count.to_s
      end
    end
  end

  GID_BASE_BY_INSTITUTION = {
     "duke.edu" => 2000000,
     "campbell.edu" => 2010000,
     "davidson.edu" => 2020000,
     "nccu.edu" => 2030000
  }

  def ldap_update(user, a_posix)
    ldap_pw = File.readlines("#{Rails.root}/config/keys/ldap", chomp: TRUE)[0]
    ldap_location = File.readlines("#{Rails.root}/config/keys/ldap_location", chomp: TRUE)[0]
    ldap = Net::LDAP.new  :host => ldap_location,
                          :port => 389,
                          :auth => {
                            :method => :simple,
                            :username => "cn=admin, dc=ncshare, dc=org",
                            :password => ldap_pw
                          }

    # get the UIDs already in use
    filter = Net::LDAP::Filter.eq("uid", "*")
    treebase = "ou=People,dc=ncshare, dc=org"
    uidarray = []
    ldap.search(:base => treebase, :filter => filter) do |entry|
      #  puts "DN: #{entry.dn}"
      entry.each do |attribute, values|
        if "#{attribute}" == "uidnumber" then
          uidarray.push( "#{values[0]}".to_i)
        end
      end
    end
    
    # get the GIDs already in use
    treebase = "ou=Group,dc=ncshare, dc=org"
    gidarray = []
    ldap.search(:base => treebase) do |entry|
      #  puts "DN: #{entry.dn}"
      entry.each do |attribute, values|
        if "#{attribute}" == "gidnumber" then
          gidarray.push( "#{values[0]}".to_i)
        end
      end
    end
    
    
    filter = Net::LDAP::Filter.eq("uid", "#{a_posix[:gitlab_user]}")
    treebase = "ou=People,dc=ncshare, dc=org"
    existing_entry = ldap.search(:base => treebase, :filter => filter)
    if existing_entry.length == 0         
      # we need to create a new group and user entry - get the unique ID to use
      next_uid = next_id_for_array(GID_BASE_BY_INSTITUTION[a_posix[:institution]], uidarray)
      next_gid = next_id_for_array(GID_BASE_BY_INSTITUTION[a_posix[:institution]], gidarray)
      
      # add the group for the user
      dn = "cn=#{a_posix[:gitlab_user]},ou=Group,dc=ncshare,dc=org"
      gattr = {
        :gidNumber => "#{next_gid}",
        :cn => "#{a_posix[:gitlab_user]}",
        :objectClass => ["posixGroup", "top"]
      }
      ldap.add( :dn => dn, :attributes => gattr )
      ldap_result = ldap.get_operation_result
      ActivityLog.log(session[:current_user_id], 'LDAP group', "#{ldap_result[:dn]} - #{ldap_result[:update_result]}")

      # add the user
      dn = "uid=#{a_posix[:gitlab_user]},ou=People,dc=ncshare,dc=org"
      the_attr = {
        :objectclass => ["top", "account", "posixAccount", "ldapPublicKey", "eduPerson"],
        :uid => "#{a_posix[:gitlab_user]}",
        :cn => "#{user[:displayName]}",
        :gecos => "#{user[:displayName]}",
        :eduPersonPrincipalName => "#{user[:netid]}",
        :loginShell => "#{a_posix[:shell_preference]}",
        :gidNumber => "#{next_gid}",
        :homeDirectory => "/hpc/home/#{a_posix[:gitlab_user]}",
        :uidNumber => "#{next_uid}",
        :sshPublicKey => "#{a_posix[:gitlab_public_key]}"
      }

      ldap.add( :dn => dn, :attributes => the_attr )
      ldap_result = ldap.get_operation_result
      ActivityLog.log(session[:current_user_id], 'LDAP user', "#{ldap_result[:dn]} - #{ldap_result[:update_result]}")
      return {:dn=> dn, :update_result=> ldap.get_operation_result }
    else 
      # entry exists, so update a few attributes in LDAP
      #
      dn = "uid=#{a_posix[:gitlab_user]},ou=People,dc=ncshare,dc=org"
      ops = [ [:replace, :loginShell, "#{a_posix[:shell_preference]}"],
              [:replace, :sshPublicKey, "#{a_posix[:gitlab_public_key]}"] ]
      ldap.modify :dn => dn, :operations => ops
    end
    return {:dn => dn, :update_result => ldap.get_operation_result}
  end

  def update
    form_values = params[:posix_user]
    gitlab_info = fetch_from_gitlab( form_values[:gitlab_user] )
    if gitlab_info[:error].nil?
      # looks good - update/create the user
      #
      user = User.find(session[:current_user_id])
      a_posix = PosixUser.find_or_create_by(user: user)
      a_posix[:institution] = user[:netid].split("@").last 
      a_posix[:gitlab_user] = form_values[:gitlab_user]
      a_posix[:gitlab_displayname] = gitlab_info[:display_name] 
      a_posix[:gitlab_public_key] = gitlab_info[:key]
      a_posix[:shell_preference] = form_values[:shell_preference]
      a_posix[:ldap_entry] = nil # fix this when we know the ldap_entry
      a_posix[:posix_active] = true
      ldap_result = ldap_update(user, a_posix)
      a_posix[:ldap_entry] = ldap_result[:dn]
      ActivityLog.log(session[:current_user_id], 'LDAP', "#{ldap_result[:dn]} - #{ldap_result[:update_result]}")
      a_posix.save
      flash[:res_notice] = "<b>updated:</b> #{user[:netid]} <br><b>gitlab.com user:</b> #{a_posix[:gitlab_user]}<br><b>shell:</b> #{a_posix[:shell_preference]}<br><b>SSH public key:</b> #{a_posix[:gitlab_public_key]}"
      ActivityLog.log(session[:current_user_id], 'POSIX update',"#{a_posix[:gitlab_user]}\t#{a_posix[:shell_preference]}\t#{a_posix[:gitlab_public_key]}")
      redirect_to posix_index_path 
    else
      redirect_to posix_index_path, alert: " #{gitlab_info[:error]}"
    end 
  end

end
