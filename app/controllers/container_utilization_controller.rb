class ContainerUtilizationController < ApplicationController
  layout 'ssiapp/application'
  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin
  
  
  def check_container_utilization
    # get all the container types and total counts
    sql_appnames = "select appname, count(*) as total_count from dockers group by appname order by count(*) desc"
    appnames = Docker.find_by_sql(sql_appnames)
    current_use_summary = {}
    appnames.each do | container_type |
      current_use_summary[container_type.appname] = {:app=>container_type.appname, :total_count=>container_type.total_count, :free=>0, :reserved=>0, :powered_down=>0}
    end
    # available to be reserved counts
    sql_free = "select appname, count(*) as free from dockers where netid = '' group by appname order by appname"
    free = Docker.find_by_sql(sql_free)
    free.each do | item |
      current_use_summary[item.appname][:free] = item.free
    end
    # currently reserved 
    sql_reserved = "select appname, count(*) as reserved from dockers where netid <> 'powered_down' and netid <> '' group by appname order by appname"
    reserved = Docker.find_by_sql(sql_reserved)  
    reserved.each do | item |
      current_use_summary[item.appname][:reserved] = item.reserved
    end
    # powered off
    sql_powered_down = "select appname, count(*) as powered_down from dockers where netid = 'powered_down' group by appname order by appname"
    powered_down = Docker.find_by_sql(sql_powered_down)
    powered_down.each do | item |
      current_use_summary[item.appname][:powered_down] = item.powered_down
    end
    
    return current_use_summary
  end
  
  def index
    @use_summary = self.check_container_utilization
  end

end
