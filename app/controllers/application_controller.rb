class ApplicationController < ActionController::Base
  layout 'ssiapp/application'
  before_action -> { flash.now[:notice] = flash[:notice].html_safe if flash[:html_safe] && flash[:notice] }
  before_action -> { flash.now[:alert] = flash[:alert].html_safe if flash[:html_safe] && flash[:alert] }

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :saml_wayf_set, except: [:home]
  before_action :saml_protect, except: [:home]
  before_action :set_user
#  around_action :set_current_user

#  before_action :saml_protect unless Rails.env.test?
#  before_action :set_user unless Rails.env.test?
  
  def onlyAdmin
    denied unless Admin.is_admin(@current_user.netid)
  end

  # def set_user
  #   session[:saml_attributes]
  #   logger.info("saml is #{session[:saml_attributes].inspect}")
  #   @eppn = session[:saml_attributes]['eduPersonPrincipalName'].first
  #   @affils = session[:saml_attributes]['eduPersonScopedAffiliation']
  # end

  def set_user
    @current_user = User.find(session[:current_user_id]) if session[:current_user_id]
  end

  private
  
  def saml_wayf_set
    if session[:wayf].nil?
      path = request.original_url.split('https://cmgr.ncshare.org/')[1]
      redirect_to  "/incommon_login/#{path}"
    end
    SamlCamel::Transaction.set_instance_wayf(session[:wayf])
  end
  

  def login_required
    set_user
    login_and_continue(request.original_url) if @current_user.blank?
  end

  def login_and_continue(url)
    group_list = ''
    netid = ''
    if false && (['development'].include? ENV['RAILS_ENV']) 
      # netid = 'lw299'
      # netid = 'liz'
      # netid = 'mccahill'
    else
      session[:saml_attributes]
      logger.info("saml is #{session[:saml_attributes].inspect}")
      @eppn = session[:saml_attributes]['eduPersonPrincipalName'][0]
      
      netid = @eppn
      group_list = request.env['ismemberof']
    end

    @user = User.find_by_netid(netid)
    if @user.blank?
      @user = User.new
      @user.netid = netid
      @user.displayName = session[:saml_attributes]['displayName'].first unless session[:saml_attributes]['displayName'].nil?
      @user.email = session[:saml_attributes]['mail'].first unless session[:saml_attributes]['mail'].nil?
      @user.phone = 'placeholder'
      @user.phone = session[:saml_attributes]['telephoneNumber'].first unless session[:saml_attributes]['telephoneNumber'].nil? 
      @user.save 

      ResourceLogger.error("Error saving user: #{@user.errors.full_messages.join(',')}") unless @user.errors.blank?
      @alert << ("Error saving user: #{@user.errors.full_messages.join(',')}") unless @user.errors.blank?
      if @user.id.blank?
        flash[:alert] = 'There was a problem creating your account.'
        return render 'home/index'
      end
    end

    # Save the user ID in the session so it can be used in
    # subsequent requests
    session[:current_user_id] = @user.id
    redirect_to url
  end


  def set_current_user
    User.as_user(@current_user) do
      yield
    end
  end

  def denied
    render 'layouts/no_access', layout: false, status: :forbidden
  end

  def require_admin
    denied unless @current_user.admin
  end

  def login_again
    render 'home/index', notice: 'You need to login again'
  end

  # Method to white-list order for table sorting
  def direction
    %w[asc desc].include?(params[:order]) ? params[:order] : 'asc'
  end
end
