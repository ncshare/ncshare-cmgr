class ActivityLogsController < ApplicationController

  before_action :login_required
  before_action :set_user  #If they have logged in
  before_action :onlyAdmin

  active_scaffold :"activity_log" do |conf|
    conf.actions = [:show, :list, :search]
    conf.list.per_page = 150
    columns[:user].weight = 0
    conf.list.sorting = { :id => :desc }
    config.columns = [:user, :action, :notes, :occurred_at]
    list.columns.exclude :created_at, :updated_at
  end

end
