class ContainersController < ApplicationController

  before_action :login_required
  before_action :set_user  #If they have logged in

  before_action :use_unsafe_params, only: [:show]

  def params
    @_dangerous_params || super
  end

require 'rest_client'
require 'net/ssh'

  def scan_for_user_containers( this_netid )
    this_user_app_reservations = Docker.current_reservations( this_netid )
    this_user_app_available = Docker.available_to_reserve( this_netid )
    return this_user_app_reservations, this_user_app_available       
  end
     
  def index
    @user = User.find session[:current_user_id]
    @the_reservations, @the_apps_available = scan_for_user_containers(@user.netid)
    respond_to do |format|
      format.html # index.html.erb
    end
  end
  
  
  def show_panel
    @user = User.find session[:current_user_id]
    # @the_reservations = Docker.current_reservations(@user.netid)
    @panel_reservation = Docker.specific_reservation(@user.netid, params[:id])
    respond_to do |format|
      format.html 
    end
  end

  def renew
    user = User.find session[:current_user_id]
    containertype = params[:containertype]
    
    expired_instance = Docker.find_by_netid_and_appname(user.netid, containertype) 
    expired_instance.expired = false
    expired_instance.save

    # the same weird problem (in production only) where Rails is not generating a valid WHERE clause
    # when we call save on the instance is happening here and in the show method.  
    # I am again resorting to updating the record via my own SQL statement.
    # here is what was not working: 
    #         
    #          expired_instance.save
    # 
    # to avoid this, get a connection to the database, and sent it some SQL the old fashioned way 
    #
    #db_connection = ActiveRecord::Base.connection
    #db_connection.execute("UPDATE dockers SET expired = FALSE, updated_at = '#{Time.now.utc}' WHERE dockers.id = #{expired_instance.id}")

    ActivityLog.log(session[:current_user_id], 'Docker renew',"Docker: #{user.netid} renewed #{containertype} at #{expired_instance.host}:#{expired_instance.port}" )                
  end
  
    
  def show
    user = User.find(session[:current_user_id])
    # container_type = request.path.split("/").last
    #
    # to accomodate the path appendix for jupyter git_pull we need to be careful to get the right type
    #
#    puts("********** #{request.original_url} ***********")
#    puts request.params.inspect
    container_type = request.path.split('/containers/')[1].split('/')[0]
    if ((container_type == 'requestrestart_rstudio') ||
        (container_type == 'requestrestart_nccu-rstudio') ||
        (container_type == 'requestrestart_davidson-csc-110-0') ||
        (container_type == 'requestrestart_davidson-pol-182-a') ||
        (container_type == 'requestrestart_pytorch') ||
        (container_type == 'requestrestart_nccu-pytorch') ||
        (container_type == 'requestrestart_davidson-vscode') ||
        (container_type == 'requestrestart_knime') ||
        (container_type == 'requestrestart_jupyter') ||
        (container_type == 'requestrestart_pyroot') ||
        (container_type == 'requestrestart_nccu-jupyter') ||
        (container_type == 'requestrestart_davidson-mat-315-0')) then
      self.requestrestart
    elsif ((container_type == 'restart_rstudio') ||
           (container_type == 'restart_nccu-rstudio') ||
           (container_type == 'restart_davidson-csc-110-0') ||
           (container_type == 'restart_davidson-pol-182-a') ||
           (container_type == 'restart_pytorch') ||
           (container_type == 'restart_nccu-pytorch') ||
           (container_type == 'restart_davidson-vscode') ||
           (container_type == 'restart_knime') ||
           (container_type == 'restart_jupyter') ||
           (container_type == 'restart_pyroot') ||
           (container_type == 'restart_nccu-jupyter') ||
           (container_type == 'restart_davidson-mat-315-0'))
      self.restart
    elsif container_type == 'renew' then
      self.renew
    elsif container_type == 'download_rdp' then
      self.download_rdp
    else
      @first_visit = true
      reserved_instance = Docker.where("netid = ? and lower(appname) = ?", user.netid, container_type).first
      if reserved_instance.nil? then # they have never reserved an instance
        new_instance = Docker.find_by_netid_and_appname('', container_type)
        if new_instance.nil? then 
            logger.error "Failed to reserve Docker #{container_type} instance for user: #{user.netid}"
            flash[:error] = "ERROR: Cannot find an unused Docker container for #{container_type}"
            redirect_to "/containers" and return
        else
          # reserve this instance
          #
          # there is a weird problem (in production only) where Rails is not generating a valid WHERE clause
          # when we call save on the new_instance. After spending enough time to convince myself I am not going
          # to figure this out, I am resorting to updating the record via my own SQL statement.
          # here is what was not working: 
          #         
          #          new_instance.save
          # 
          # to avoid this, get a connection to the database, and sent it some SQL the old fashioned way 
          # 
          new_instance.netid = user.netid
          new_instance.save
          #db_connection = ActiveRecord::Base.connection
          #db_connection.execute("UPDATE dockers SET netid = '#{new_instance.netid}', updated_at = '#{Time.now.utc}' WHERE dockers.id = #{new_instance.id}")
          
          @docker_host = new_instance.host
          @docker_port = new_instance.port
          @docker_guest_pw = new_instance.pw  
          ActivityLog.log(session[:current_user_id], 'Docker containers',"Docker: #{user.netid} reserved #{container_type} at #{@docker_host}:#{@docker_port}" )                
          flash[:error] = "Reserved a container for #{container_type} -- to start a new session, click on the #{container_type} link in the list below."
          redirect_to "/containers" and return
        end
      else #send them to the instance they previously reserved
        @first_visit = false
        @docker_host = reserved_instance.host
        @docker_port = reserved_instance.port
        @docker_guest_pw = reserved_instance.pw    
      end 
      if container_type == 'rstudio'           
        # no longer will be use RStudio's hack-o-rama Javascript public key crypto on a form
        # instead we will  use an Nginx auth_proxy to check to port and pw and decide if we should let the user in
        #@docker_host_public_key = RestClient.get( "https://#{@docker_host}:#{@docker_port}/auth-public-key")
      elsif container_type == 'nccu-rstudio'           
        # no longer will be use RStudio's hack-o-rama Javascript public key crypto on a form
        # instead we will  use an Nginx auth_proxy to check to port and pw and decide if we should let the user in
        #@docker_host_public_key = RestClient.get( "https://#{@docker_host}:#{@docker_port}/auth-public-key")
      elsif container_type == 'davidson-csc-110-0'           
        # no longer will be use RStudio's hack-o-rama Javascript public key crypto on a form
        # instead we will  use an Nginx auth_proxy to check to port and pw and decide if we should let the user in
        #@docker_host_public_key = RestClient.get( "https://#{@docker_host}:#{@docker_port}/auth-public-key")
      elsif container_type == 'davidson-pol-182-a'           
        # no longer will be use RStudio's hack-o-rama Javascript public key crypto on a form
        # instead we will  use an Nginx auth_proxy to check to port and pw and decide if we should let the user in
        #@docker_host_public_key = RestClient.get( "https://#{@docker_host}:#{@docker_port}/auth-public-key")
      elsif container_type == 'pytorch'
        # Pytorch notebook-specific code here 
      elsif container_type == 'nccu-pytorch'
        # Pytorch notebook-specific code here 
        # to accomodate git-pull and other hacks supported by jupyterhub
        if params[:redirect].nil? then
          @pathappendix = "?next=%2Flab"
        else
          # tack the user-redirect info onto the tail end of the path
          @pathappendix = "?next=" + request.original_url.split('pytorch?redirect=')[1]
        end
#        puts("---------- #{@pathappendix} ----------")
      elsif container_type == 'davidson-vscode'
        # VSCode notebook-specific code here 
      elsif container_type == 'knime'
        # Knime notebook-specific code here 
      elsif container_type == 'jupyter' then
        # jupyter/iPython notebook-specific code here 
        # to accomodate git-pull and other hacks supported by jupyterhub
        if params[:redirect].nil? then
          @pathappendix = "?next=%2Flab"
        else
          # tack the user-redirect info onto the tail end of the path
          @pathappendix = "?next=" + request.original_url.split('jupyter?redirect=')[1]
        end
#        puts("---------- #{@pathappendix} ----------")
      elsif container_type == 'pyroot' then
        # jupyter/iPython notebook-specific code here 
        # to accomodate git-pull and other hacks supported by jupyterhub
        if params[:redirect].nil? then
          @pathappendix = "?next=%2Flab"
        else
          # tack the user-redirect info onto the tail end of the path
          @pathappendix = "?next=" + request.original_url.split('pyroot?redirect=')[1]
        end
#        puts("---------- #{@pathappendix} ----------")
      elsif container_type == 'nccu-jupyter' then
        # jupyter/iPython notebook-specific code here 
        # to accomodate git-pull and other hacks supported by jupyterhub
        if params[:redirect].nil? then
          @pathappendix = "?next=%2Flab"
        else
          # tack the user-redirect info onto the tail end of the path
          @pathappendix = "?next=" + request.original_url.split('jupyter?redirect=')[1]
        end
#        puts("---------- #{@pathappendix} ----------")
      elsif container_type == 'davidson-mat-315-0' then
        # jupyter/iPython notebook-specific code here 
        # to accomodate git-pull and other hacks supported by jupyterhub
        if params[:redirect].nil? then
          @pathappendix = "?next=%2Flab"
        else
          # tack the user-redirect info onto the tail end of the path
          @pathappendix = "?next=" + request.original_url.split('jupyter?redirect=')[1]
        end
#        puts("---------- #{@pathappendix} ----------")
      end
      ActivityLog.log(session[:current_user_id], 'Docker containers',"Docker: #{user.netid} logging into #{container_type} at #{@docker_host}:#{@docker_port} #{@pathappendix}" )    
    end
    render :template => "containers/#{container_type.downcase}" 
  end
  
  
  
  def requestrestart
    user = User.find(session[:current_user_id])
    container_type = (request.path.split("/").last).split("_").last
    reserved_instance = Docker.where("netid = ? and lower(appname) = ?", user.netid, container_type).first
    if reserved_instance.nil? then 
      # they do not have a reserved instance - do nothing
    else # use the instance they previously reserved
      @first_visit = false
      @docker_host = reserved_instance.host
      @docker_port = reserved_instance.port
      @docker_guest_pw = reserved_instance.pw  
      @restart_container_path = "restart_#{container_type}" 
      if ((container_type == 'rstudio') ||
          (container_type == 'nccu-rstudio') ||
          (container_type == 'davidson-csc-110-0') ||
          (container_type == 'davidson-pol-182-a') ||
          (container_type == 'pytorch') ||
          (container_type == 'nccu-pytorch') ||
          (container_type == 'davidson-vscode') ||
          (container_type == 'knime') ||
          (container_type == 'jupyter') ||
          (container_type == 'pyroot') ||
          (container_type == 'nccu-jupyter') ||
          (container_type == 'davidson-mat-315-0'))
          # they requested a restart
          ActivityLog.log(session[:current_user_id], 'Docker containers',"Docker: #{user.netid} request restart #{container_type} at #{@docker_host}:#{@docker_port}" )    
      end
    end
  end
  
  def do_ssh(opts)
      default_opts = {
        :keys => "#{Rails.root}/config/keys/cmgruser_ed25519",
        :keys_only => true,
        :valid_exit_codes => [0],
      }
      opts = default_opts.merge opts
      output = ""
      command = opts.delete(:command)
      valid_exit_codes = opts.delete(:valid_exit_codes)
      Net::SSH.start(nil, nil, opts) do |ssh|
        ssh.open_channel do |channel|
          channel.exec(command) do |ch, success|
            raise "could not execute command: #{command}" unless success
            channel.on_data { |c,data| output+=data }
            channel.on_extended_data { |c,type,data| output+=data }

            channel.on_request("exit-status") do |c,data|
              exit_code = data.read_long
              raise "#{command} failed with exit #{exit_code}: #{output}" if ! valid_exit_codes.include? exit_code
            end

            channel.on_request("exit-signal") do |c, data|
              raise "#{command} died with signal #{data.read_string}: #{output}"
            end
          end
        end
        ssh.loop
        return output
      end
      nil
  end

  def restart
    user = User.find(session[:current_user_id])
    container_type = (request.path.split("/").last).split("_").last
    reserved_instance = Docker.where("netid = ? and lower(appname) = ?", user.netid, container_type).first
    if reserved_instance.nil? then 
      # they do not have a reserved instance - do nothing
    else # use the instance they previously reserved
      @first_visit = false
      @docker_host = reserved_instance.host
      @docker_port = reserved_instance.port
      @docker_guest_pw = reserved_instance.pw  
      @restart_error = nil 
      if ((container_type == 'rstudio') || 
          (container_type == 'nccu-rstudio') || 
          (container_type == 'davidson-csc-110-0') || 
          (container_type == 'davidson-pol-182-a') || 
          (container_type == 'pytorch') || 
          (container_type == 'nccu-pytorch') || 
          (container_type == 'davidson-vscode') || 
          (container_type == 'knime') || 
          (container_type == 'jupyter') || 
          (container_type == 'pyroot') || 
          (container_type == 'nccu-jupyter') || 
          (container_type == 'davidson-mat-315-0'))
          # we are going to do a restart
          ActivityLog.log(session[:current_user_id], 'Docker containers',"Docker: remote restart call for #{user.netid} restart #{container_type} at #{@docker_host}:#{@docker_port}" )   
          begin
            # get the correct remote user to run the restart command
            if ((container_type == 'rstudio') ||
                (container_type == 'nccu-rstudio') ||
                (container_type == 'davidson-csc-110-0') ||
                (container_type == 'davidson-pol-182-a') ||
                (container_type == 'pytorch') ||
                (container_type == 'nccu-pytorch') ||
                (container_type == 'davidson-vscode') ||
                (container_type == 'knime') ||
                (container_type == 'jupyter') ||
                (container_type == 'pyroot') ||
                (container_type == 'nccu-jupyter') ||
                (container_type == 'davidson-mat-315-0'))
              remote_restart_user = 'cmgruser'
            end
            # ActivityLog.log(session[:current_user_id], 'Docker containers',"calling do_ssh with :user => #{remote_restart_user}, :host_name => #{@docker_host} /srv/persistent-data/docker-scripts/restart-one-#{container_type} #{@docker_port}" )
            # run the restart command
            out = do_ssh(:user => "#{remote_restart_user}", :host_name => "#{@docker_host}", 
                         :command => "/srv/persistent-data/docker-scripts/restart-one-#{container_type} #{@docker_port}")
          rescue Exception => e  
            @restart_error = "ERROR trying to restart container:" + e.message.split('ERROR:').last
            ActivityLog.log(session[:current_user_id], 'Docker containers',"Docker: #{@restart_error} for #{user.netid} restart #{container_type} at #{@docker_host}:#{@docker_port}" )
          else
            ActivityLog.log(session[:current_user_id], 'Docker containers',"Docker: SUCCESSFUL RESTART #{user.netid} restart #{container_type} at #{@docker_host}:#{@docker_port}" )   
          end
      end
    end
  end


  private

  def use_unsafe_params
    @_dangerous_params = request.parameters
  end
 
end