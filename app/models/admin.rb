class Admin < ApplicationRecord
  # attr_accessible :displayName, :dukeId, :netid
  
  # ActiveScaffold will automatically use this name method to describe a
  # record of this type
  def name
    "#{netid}"
  end

  def Admin.fromEPPN(eppn)
    parts = eppn.split('@')
    if(parts[1].eql?('duke.edu'))
      return self.find_by_netid(parts[0])
    else
      return nil
    end
  end
  
  def Admin.is_admin(this_netid)
    return !( self.find_by_netid(this_netid).nil?)
  end
end
