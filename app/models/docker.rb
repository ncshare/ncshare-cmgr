class Docker < ApplicationRecord
  #attr_accessible :netid, :host, :port, :pw, :appname, :appdesc
  
  before_save :fix_null_netids   

  def fix_null_netids
    # the active scaffold editor will put NULL in the netid field if we delete it
    # but our logic depends on having an empty string to indicate a free container
    # so fix netid.nil to be ''
    if self.netid.nil?  then
      self.netid = ''
    end
  end


  # ActiveScaffold will automatically use this name method to describe a 
  # record of this type
  def name
    "#{appname} : #{netid} #{port}"
  end
  
  def self.current_reservations( this_netid )
    this_user_app_reservations = []
    #docker_app_types = Docker.select("DISTINCT appname, appdesc")
    Docker.docker_app_types.each do |app_type|
      reserved_instance = Docker.find_by_netid_and_appname(this_netid, app_type.appname) 
      if !( reserved_instance.nil? ) then
        this_user_app_reservations.push(:container_type=>app_type.appname, :container_desc=> app_type.appdesc, 
        :app_path=> "containers/#{app_type.appname.downcase}",
        :app_restart_path=> "containers/restart_#{app_type.appname.downcase}",
        :app_requestrestart_path=> "containers/requestrestart_#{app_type.appname.downcase}",
        :app_expired=> reserved_instance.expired,
        :app_renew_path=> "containers/renew?containertype=#{app_type.appname.downcase}",
        :docker_id=> reserved_instance.id,
        :first_visit=> false )
      end     
    end 
    return this_user_app_reservations           
  end

  def self.specific_reservation(this_netid, this_id)  
    this_reservation = nil
    reserved_instance = Docker.find_by_netid_and_id(this_netid, this_id) 
    if !( reserved_instance.nil? ) then
      this_reservation = {
        :container_type=>reserved_instance.appname, 
        :container_desc=> reserved_instance.appdesc, 
        :app_path=> "/containers/#{reserved_instance.appname.downcase}",
        :app_restart_path=> "/containers/restart_#{reserved_instance.appname.downcase}",
        :app_requestrestart_path=> "/containers/requestrestart_#{reserved_instance.appname.downcase}",
        :app_renew_path=> "/containers/renew?containertype=#{reserved_instance.appname.downcase}",
        :app_expired=> reserved_instance.expired,
        :docker_id=> reserved_instance.id,
        :first_visit=> false 
      }
    end     
    return this_reservation          
  end
  
  def self.available_to_reserve( this_netid )
    this_user_app_available = []
    #docker_app_types = Docker.select("DISTINCT appname, appdesc")
    Docker.docker_app_types.each do |app_type|
      reserved_instance = Docker.find_by_netid_and_appname(this_netid, app_type.appname) 
      if reserved_instance.nil? then
        not_reserved = ''
        open_instance = Docker.find_by_netid_and_appname(not_reserved, app_type.appname) 
        this_user_app_available.push(:container_type=>app_type.appname, :container_desc=> app_type.appdesc, 
                                     :app_path=> "containers/#{app_type.appname.downcase}",
                                     :first_visit=> true ) unless open_instance.nil?
      end     
    end 
    return this_user_app_available       
  end
  
  def self.docker_app_types
    # return Docker.select("DISTINCT appname, appdesc").order(:appname).reverse
    # 
    # fancy logic to return docker items ordered by number of containers
    # we need this now that there are a lot of containers and unpopular 
    # ones were at the top of the list
    #
    disordered_results = Docker.select("DISTINCT appname, appdesc")
    ordered_results = []
    Docker.group("appname").order('count_appname DESC').count('appname').keys.each do |item|
      disordered_results.each do |thing| 
        if ( thing.appname == item ) then
          ordered_results.push( thing )
    	end
      end
    end
    return ordered_results
  end
  
  
end
