class ActivityLog < ApplicationRecord
  #attr_accessible :action, :user_id, :notes, :occurred_at
  
  belongs_to :user

  # ActiveScaffold will automatically use this name method to describe a
  # record of this type
  def name
    "#{occurred_at}"
  end


  
  def self.per_page
    40
  end
  
  def ActivityLog.log(user_id,action, noteStr)
    @activity = self.new(:user_id=>user_id, 
      :action=>action,
      :notes=>noteStr,
      :occurred_at=>Time.now)
    @activity.save
    begin
      user = User.where(id: user_id)
      user = user.blank? ? "Missing" : user.first.to_s
      SplunkLogger.info("User: #{user}, Action: #{action} - #{noteStr} ")
      
    rescue Exception => e
      
    end
  end
  
end
