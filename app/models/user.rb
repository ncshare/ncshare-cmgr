class User < ApplicationRecord
  #attr_accessible :displayName, :duid, :email, :netid, :phone, :vms_allowed
        
  has_many :activity_logs
  
  validates :displayName, :presence => true  
  validates :email, :presence => true
  validates :phone, :presence => true
  validates :netid, :presence => true

  # ActiveScaffold will automatically use this name method to describe a
  # record of this type
  def name
    "#{netid}"
  end


  ######## CLASS Methods
  def self.as_user(user)
    @current_user = user
    yield
  end

  def self.current_user
    @current_user.presence || User.system_user
  end

  def self.from_netid(netid)
    user = find_by(netid: netid)
    user || LdapTool.createUser(netid)
  end

  ########

  
  def to_s
    return displayName.blank? ? netid : displayName
  end
 
end
