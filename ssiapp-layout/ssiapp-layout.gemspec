# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ssiapp/layout/version'

Gem::Specification.new do |spec|
  spec.name          = "ssiapp-layout"
  spec.version       = Ssiapp::Layout::VERSION
  spec.authors       = ["Sean Dilda"]
  spec.email         = ["sean@duke.edu"]
  spec.summary       = %q{Standard layout for SSI Rails applications}
  spec.description   = %q{Standard layout for SSI Rails applications}
  spec.homepage      = ""
  spec.license       = "Proprietary - contains Duke copyrighted logos"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
