# Helper to tidy up the main ssiapp application.html.haml
module Ssiapp::ApplicationHelper

  def app_class_name
    # Different versions of rails use different methods to retrieve this, so try the
    # name used by the latest version and fall back on older versions if needed
    rails_class = Rails.application.class
    return rails_class.module_parent_name if rails_class.respond_to?(:module_parent_name)

    rails_class.parent_name
  end
end
