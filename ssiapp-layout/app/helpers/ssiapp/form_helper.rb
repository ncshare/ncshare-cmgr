module Ssiapp::FormHelper

  def form_group attribute:, errors:, warnings:[], label:, form:, error_attribute:nil, content_class:nil, &block
    content_class ||= 'col-sm-4'
    if error_attribute.nil?
      error_attribute = attribute
    end

    field = capture(&block)

    classes = ['form-group']
    unless errors.empty? && warnings.blank?
      classes << 'has-feedback'
      if errors.include?(error_attribute)
        classes << 'has-error'
        field += content_tag(:span, '', 'aria-hidden' => true, class: ['glyphicon', 'glyphicon-remove', 'form-control-feedback'])
      elsif warnings.include?(error_attribute)
        classes << 'has-warning'
        field += content_tag(:span, '', 'aria-hidden' => true, class: ['glyphicon', 'glyphicon-warning-sign', 'form-control-feedback'])
      else
        classes << 'has-success'
        field += content_tag(:span, '', 'aria-hidden' => true, class: ['glyphicon', 'glyphicon-ok', 'form-control-feedback'])
      end
    end

    content_tag(:div, class: classes) do
      form.label(attribute, label, class: ['col-sm-2', 'control-label']) +
      content_tag(:div, field, class: content_class)
    end
  end

end
